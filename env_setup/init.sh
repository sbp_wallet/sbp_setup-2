#!/usr/bin/env bash

# ** user setup
cp  /home/www/sbp_setup/env_setup/config-sample/common/sbp.atm /etc/.sbp.atm

# ** sbp client & printer services setup
mkdir /opt/app/bin -p
chmod a+x /opt/app/bin -R
mkdir /opt/app/tmp/sbp/ -p
chmod a+x /opt/app/tmp/sbp/ -R

tar zxvf /home/www/sbp_setup/env_setup/config-sample/common/sbpclientservice-0.04-i486-2.tgz -C /
tar zxvf /home/www/sbp_setup/env_setup/config-sample/common/sbpprinterservice-1.01-i686-1.tgz -C /
ln -sf /opt/app/bin/ClientService.py /usr/local/bin
ln -sf /opt/app/bin/SbpPrintingService.py /usr/local/bin


# ** stunnel setup
cp /home/www/sbp_setup/env_setup/config-sample/common/generate-stunnel-key.sh.sample /etc/stunnel/generate-stunnel-key.sh
cp /home/www/sbp_setup/env_setup/config-sample/common/stunnel.cnf.sample /etc/stunnel/stunnel.cnf
cp /home/www/sbp_setup/env_setup/config-sample/staging/stunnel.conf.sample /etc/stunnel/stunnel.conf
chmod a+x /etc/stunnel/*
cd /etc/stunnel/
./generate-stunnel-key.sh
killall stunnel4 && sleep 2 && stunnel4
