#!/usr/bin/env bash

killall stunnel4
cp /home/www/sbp_setup/env_setup/config-sample/local/stunnel.conf.sample /etc/stunnel/stunnel.conf
cp /home/www/sbp_setup/env_setup/config-sample/local/cliconfig.py.sample /etc/sbp/cliconfig.py
sleep 1
stunnel4

kill $(ps aux | grep 'ClientService.py' | awk '{print $2}')
ClientService.py &
kill $(ps aux | grep 'SbpPrintingService.py' | awk '{print $2}')
SbpPrintingService.py &